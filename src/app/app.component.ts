import { Component } from '@angular/core';
import { FlatpickrOptions } from 'ng2-flatpickr';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  data: any[] = [
    {
       item: "banana"
    },
    {
       item: "apple"
    },
    {
       item: "lemon"
    }
  ]

  options: FlatpickrOptions = {
    enableTime: true,
    minDate: new Date(),
    dateFormat: "m-d-Y H:i"
  }

  addSkill(): void {
    this.data.push({});
  }

  onClick(e): void {
    console.log(e.target.value)
    console.log(e)
    //console.log(document.getElementsByClassName('hello').value)
  }

}
